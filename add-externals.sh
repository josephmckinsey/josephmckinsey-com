#!/bin/sh

cp external-papers/bonferroni/main.rst content/bonferroni.rst
cp external-papers/pythagorean-sans-verbis/main.rst content/pythagorean-sans-verbis.rst
cp external-papers/pythagorean-sans-verbis/pythagoreanTheorem.svg content/
cp external-papers/pythagorean-sans-verbis/pythagoreanTheorem2.svg content/
cp external-papers/pythagorean-sans-verbis/pythagoreanTheorem3.svg content/
cp external-papers/pythagorean-sans-verbis/perpendicularbisector.svg content/

