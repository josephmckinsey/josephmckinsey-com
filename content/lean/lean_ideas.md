Title: Ideas for things to do in Lean
Date: 2025-01-20 20:37
Category: Notes
Status: hidden

1. We reimplement floating point numbers in lean so we can use trusted computations about natural numbers to still do arithmetic in tactics and such.

SciLean has functions parameterized over real scalar-like types, which can
be real or floating point. Proving equivalence up to a bound
is the problem. By default it's not even composable.

2. The idea of using filters is pretty convenient for representing convergence, but one problem is that it isn't very computational.

Is there a computational flavor of filters that gives you precise bounds?

For example, if f : \N -> \R converges to 2 at infinity, then I'd like to know how fast ... i.e. given the epsilon neighborhood at 2, give me the n which the filter maps everything above to within that neighborhood.

Can filters be adapted like that to include the actual functions somehow?

I'm thinking of having your elements in your filter be a special mapping from a constructible preorder. Like map epsilon to sets. Map n to the set above n. Then you just need a map from one index on your filter to another index on your filter. That would then guarantee the original result for filters.

Indexed filters? Maybe someone has already thought of this. Or maybe it has some obvious problems.

Still not sure about the property required for the index. Obv it needs to induce a regular filter map.

Or something like that?

Maybe just sigma types and FilterBasis.

3. Better support for CAS. Or a lean CAS.

SciLean has automatic differentiation. It's a bit syntactic.

Polyrith is another thing where it goes out to sagemath for computations.

4. Better widgets for common math objects. What does a group look like?

See monoidal category ProofWidget

5. Reimplement Coq and Isabelle theories

Lean is stronger, right?

6. Von Neumann Axioms for Utility Functions

7. Risk measures

8. Develop the theory of "finite maps" in a way that can be applied to HashMaps

HashMaps \alpha \beta give us a few functions which should be the interface instead:
- keys are a finite set. Let this be called `.keys` with decidable equality (and lawful ==)
- there is a way to get a value called `get`
- h.get : h.keys \to \beta
- h.get.toMap = h
- extensionality (h.get k) = (h'.get k) for all k iff h' = h
- insert
- deletion
- hasKey

It would be neat if this worked well in a system where we can go to arrays (indexed by [0,...,n] for some n),
but this definitely changes insertion, deletion, and has key.

I propose a "FinMap" class which has the finite set of keys.

9. Fix simp confluence

- (!decide ¬q < 0) goes to !decide (0 ≤ q) instead of decide q < 0

10. nand2tetris in Lean

Maybe even incorporate `aeneas`?

11. Fill out matrix cookbook

https://github.com/eric-wieser/lean-matrix-cookbook

12. Create a unit testing library

https://leanprover.zulipchat.com/#narrow/stream/113489-new-members/topic/How.20to.20create.20and.20run.20unit.20tests.3F

Should be able to incorporate https://github.com/argumentcomputer/LSpec

This is already used in Wasm.lean for example

13. Do reflection in lean?

14. Side-angle-side rule

and other basic geometry

15. Huzita-Hatori Axioms for origami

16. Hilbert curve

17. Bonferroni inequalities

18. Common special functions in probability

19. FFI?

20. Irrational rotations of the circle are ergodic?

I feel like this should be proved already?

21. Solutions of ODEs?

22. Multidimensiona guassians

23. Ergodic theorem

24. Go through and find the least-active and smallest folders.

Then let's add to the theory there. It's probably not "complete".

25. Generalizing theorems about Cartesian Closed categories

26. Look for "TODO" in mathlib!
