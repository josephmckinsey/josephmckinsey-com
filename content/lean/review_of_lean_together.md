Title: Lean Together 2025 Reviewed
Date: 2025-01-20 20:32
Category: Review
Status: hidden
Tags: lean, math

Recently, I watched every talk I could find on the YouTube [playlist](https://www.youtube.com/playlist?list=PLlF-CfQhukNlzXdQvu1SVt9vcD4--fLlg)
for Lean Together 2025. If you don't know what Lean is, then this
post is not for you. TLDR: Lean is an interactive theorem prover,
my latest obsession, and full of interesting and half-baked ideas.
Overall, I had a lot of fun, and it's clear everyone is working hard,
even if it's not as done as I'd like.

## 1. Root systems and root data := Instructive

I don't really know what root systems are in Lie algebra, since my Lie
algebra and Lie Group skills are quite weak, but this was an excellent talk
about how definitions change when put into a unified system. The presentation
was enjoyable too.

## 2. An introduction to linters := Neat

I think this should probably be a page in the documentation instead, and I'm
not sure if that makes it a good talk or a bad talk. Ultimately linters
seem to just work for users.

## 3. Efficient Forward Reasoning for Aesop := Relevant to someone else

I'm not really making a proof hammer, and this felt like details
on the fitting of a head to a handle.

## 4. Emily Riehl, The \infty-cosmos project... := Weirdly more technical

Sometimes it feels like Eimly Riehl is still figuring out the "right" way to do
infinity category theory in Lean. This doesn't feel like it. Given she is one of
the experts in the field and wrote the basic textbooks, if she eventually figures
it out, we'll have really learned something new. Category theory is still
"hard", which in some senses is a relief to everyone who has tried learning it.

## 5. The Equational Theories Project := Less relevant than expected

Given how hacky it was they interfaced with external tools, I imagine
few long term gains will be made. The infrastructure might be bespoke,
and the actual mathematics were not explained very well in my opinion.
Neural AI had no appearance or significant application, and everything
could be sovled either by weird counterexamples or SMT solvers.

## 6. The Lean FRO Year 2 Roadmap and Vision := Exciting for the optimists

With the `grind` tactic, monadic code reasoning, a reference manual, and
reliable FFI, it's exciting to see a stabler system.

## 7. Searching for Proof Improvements with tryAtEachStep := Simple and good idea

Brought to you by the creator of `Compfiles` (IMO problem dataset), the idea of
trying out proof improvements as a periodic job on repositories is
really smart. It would be a nice linter, but I think it takes to long and
might not work.

## 8. Vertex algebras in Mathlib: coming soon? := Over my head

I didn't understand this talk at all. I would like to know what vertex algebras
are still.

## 9. Real world Autoformalization := mostly tricks about using LLMs

This was primarily about LeanAide, which doesn't seem ready for prime-time yet.
It also strikes one with the belief that something is fundamentally missing.
I'm sad to see that LLMs are stuck blindly chasing tactics instead of trying to
create their own goals

## 10. Building a Formal Verification Framework for Smart Contracts := so last season

I didn't finish watching this. It's probably good work, but I don't care about
the broader relevance.

## 11. Programming with Dependently Typed Tables in Lean := relevant to my day job

My inner data scientist cares a lot about this application, but many of the design
choices were very strange like heterogeneous lists for rows. That said, I believe
this is an excellent first step to something not only performant but also correct.
It also supports my beliefs that pivoting is bad.

## 12. The last mile := preach, man!

This guy knows what users and AI researchers should actually be focused on to
create something really useful. He not only blames researchers but also users.
Blame the user! You are also left with the impression we are not near the limits
of traditional symbolic AI.

## 13. Automated Bit-Level Reasoning in Lean 4 := really clever and useful

This has opened my eyes on how reflective programming can be used to prove things
in Lean. Let's interpret everything in a model and then use that to prove stuff.

## 14. Information theory in Lean: the DPI := lots of measure theory

Developing the "data processing inequality" requires dealing with
$\infty$ valued integrals off the beaten path, weird $f$-divergences, and
even a version of hypothesis testing. This feels like a narrow path
cut through a larger theory still.

## 15. A Nimble Introduction to Nimbers

If you know anything about nimbers, then you know that Sprague-Grundy theorem
relating impartial combinatorial games to nimber addition. On a more basic level,
this talk goes straight to ordinals, nimber addition, and even multiplication.
The constructions are almost as complicated as the surreal numbers, with
lots of open question, and still nothing developed for combinatorial game theory.

## 16. Scaling Mathlib := enticing but mostly annoying

Touts the good documentation and provides no links to any of its cool tools
it talks about (like where is `lake exe refactor` documentation?!). Seems like
the key problems are still PR review and type class inference.

## 17. Progress report on the Carleson Project := glimpse into hard harmonic analysis

It turns out proving almost everywhere pointwise convergence of the Fourier transform
is actually really hard and not even true for L1. The Carleson Project
is formalizing that it works while the talk mentions how design decisions really
change convenience like bundled vs not for Lp, or the various types of reals.

## 18. Egg: An Equality Saturation Tactic in Lean := still experimental `:(`

Equality saturation is expected to do pretty good compared to something like simp,
but it seems like this isn't as usable for a couple reasons. There's a big problem
of how to select the 30 or so lemmas + any conditionals needed. There's also
no cacheing or "Try this:". Plus you need Rust and an unstable research project.

## 19. Verified Foundations for Differential Privacy := neat but weird and a bit concerning

The way that they model randomness is very strange to me. They seem
to work in a sort of monad that lets them appear to sample, but really
they are computing with distributions and using metaprogramming to go to C++
with a different interpretation. Considering he said he doesn't know
what `Filter`s are, that makes me very wary, but he also just applied
`jacobiTheta₂_term` so solve a problem is. He definitely should have
used Filters to express convergence properties of his series
of functions.

## 20. lean-SMT := mildly untrustworthy

`lean-SMT` mainly abuses `lean-auto` (which does the same thing?),
and replays the proof. Notably, this talk was more informative in that
it explains Isabelle Sledge Hammer does proof replay and a verified
checker does "reflection rathen than simulation" ike SMTCoq. Also apparently,
there is no SMT proof format, stuff like cvc5 proof rules are unstable, and
Isabelle's SledgeHammer is actually useful. Proof replay is done
not only through simple lemmas and a few custom tactics, but also verified checkers
for polynomials (https://github.com/ufmg-smite/lean-smt/blob/4cdea120ba132ba0cb817e7fd516a967f1148752/Smt/Reconstruct/Rat/Polynorm.lean#L226). Also
note that theorems he says are proved are definitely full of `sorry`s! "We can prove"
is not the same as "we proved", which is sneaky! He also defines his own
Polynomial and Monomial? His only Real example also works with `linarith`...

## 21. Toward quasi-categories in Lean := I'm lost

I don't know what quasicategories are, and I still don't really know, so
I got lost in the first 3 minutes. Oh well?

## 22. Pantograph: A Machine-to-Machine Interaction Interface for Lean 4 := `sorry`

Unavailable, per the Lean Together 2025 Zulip with Kevin Buzzard
> Leni [Aniva] didn') want her talk to be recorded because she said that the information
> given in it will be out of date soon.

## 23. Education and research experiments with formalization := ehh

This talk is really just about the prime number theorem. It seems they restricted
themselves primarily to a rectangular API in complex analysss, which is
not super promising to be honest. I had looked forward to more general tools, and
we aren't getting that here.

## 24. Formalizing the Bruhat-Tits tree := if you know about symmetric spaces in p-adic

I got lost after 5 minutes.

## 25. Braid Groups in Lean := good for learning about computing about braid groups

I didn't find it super relevant, and also I was surprised they
didn't use a more programming language-y approach by converting the problem
to one about regexes (word processing in groups).

## 26. Physics and Lean := unsatisfying to both

By being primarily concerned with particle physics, I'm not convinced
that either physicists or leaners will get into this. They do have
something about index notation for tensors, which I didn't understand.

## 27. Division polynomials and elliptic curves := easy to get lost

It started off relevatively simple about proving operations over
elliptic curves, and then it went straight into weird stuff with some
crazy definitions of $[n]P$, which I still don't know what that means.

## 28. Metaprgoramming on monoidal categories := not clever but useful

This talk is obviously useful, but the presenter was really boring.
Using the coherence conditions and stuff is notoriously difficult, and
this provides a faster normalization procedure (sadly not using reflection,
because it's slow). It also provides ProofWidgets using Penrose,
which look awesome.
