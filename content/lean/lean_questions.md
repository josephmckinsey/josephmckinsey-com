Title: Questions and Answers about Lean
Date: 2025-01-20 20:38
Category: Notes
Tags: lean, math
Status: hidden

This is my WIP compendium of lean questions and answers. Everything starts under
[Questions](#lean-questions) and hopefully ends up at [Answers](#lean-answers) once I feel I understand.
Many of these questions I have since discovered the answers to and haven't written them down yet.

[TOC]

# Lean Questions

## What is the difference between simp and rw?

rw seems to be a targeted version of simp? Can all simps be unrolled in rws?

## Why are decidable instances so slow sometimes? What's up with native_decide?

Relatedly, there's a bv_decide for bit vectors https://blog.lean##lang.org/blog/2024-10-3-lean-4120/

Decidability has to be checked by replaying of proof of reducability
to True or False. This involves either specific rules used in Decidable
instances combined with reducability and definitional equality. All of
this can be quite slow. `native_decide` runs the function and
uses the axiom `ofReduceBool` to trust the compiler.

## how are you supposed to use external languages for computation if necessary?

FFI

Generating extra code https://github.com/teorth/equational_theories/tree/main/equational_theories/Generated/FinitePoly/src

In-House: https://github.com/marcusrossel/lean-egg


## What's this symbol « used for in imports

Like import «Tactics».Sym
https://github.com/leanprover/LNSym/blob/892621962549f7f3b9a5214349a80984e14e6d9d/Tactics.lean#L8C1##L8C21

They are "double guillements" which allow you to quote an identifier in case it's
a keyword.

From https://lean-lang.org/doc/reference/latest//Source-Files/Files/#keywords-and-identifiers:

> Identifiers components may also be surrounded by double guillemets ('«' and '»'). Such identifier components may contain any character at all aside from '»', even '«', '.', and newlines. The guillemets are not part of the resulting identifier component, so «x» and x denote the same identifier. «Nat.add», on the other hand, is an identifier with a single component, while Nat.add has two.

> Some potential identifier components may be reserved keywords. The specific set of reserved keywords depends on the set of active syntax extensions, which may depend on the set of imported modules and the currently-opened namespaces; it is impossible to enumerate for Lean as a whole. These keywords must also be quoted with guillemets to be used as identifier components in most syntactic contexts. Contexts in which keywords may be used as identifiers without guillemets, such as constructor names in inductive types, are raw identifier contexts.

## What is the conv tactic

https://leanprover.github.io/theorem_proving_in_lean4/conv.html

The "conv" tactic allows you to go right into expressions
and then rewrite random stuff using congruence, extensionality, and
pattern matching

```lean
example (a b c : Nat) : a * (b * c) = a * (c * b) := by
  conv =>
    -- ⊢ a * (b * c) = a * (c * b)
    lhs
    -- ⊢ a * (b * c)
    congr
    -- 2 goals: ⊢ a, ⊢ b * c
    rfl
    -- ⊢ b * c
    rw [Nat.mul_comm]
```

## Is there a tactic cheat sheet?

https://proofassistants.stackexchange.com/questions/3986/is-there-a-complete-index-of-lean-4-tactics

You can use #help tactic (after import Mathlib.Tactic.HelpCmd) to list all tactics available in the current environment. You may also want to import Mathlib.Tactic.Common to make sure mathlib's basic tactics are available, and import Mathlib.Tactic for the kitchen sink.

There is also https://github.com/haruhisa-enomoto/mathlib4-all-tactics/blob/main/all-tactics.md, which is the result of postprocessing the output of #help tactic into markdown form.

This list is automatically generated and just contains the hover documentation for each tactic, so it's not particularly high quality (it includes some semi-internal tactics and tactic combinators, and sometimes the same tactic appears multiple times because it is internally represented as multiple tactics with the same start token and different following syntax, or slight variations on the same tactic). Lean 3 had a specifically curated tactics page but this has not been replicated in lean 4 yet.

There is also a more organized list at https://lean-lang.org/doc/reference/latest/Tactic-Proofs/Tactic-Reference/#tactic-ref-rw

There is also a whole Lean Zulip topic about this: https://leanprover.zulipchat.com/#narrow/stream/113488-general/topic/tactic.20cheatsheet

- https://florisvandoorn.com/files/lean-tactics.pdf
- https://leanprover-community.github.io/mathlib-manual/html-multi/
- Updated compilation from #help: https://seasawher.github.io/mathlib4-help/tactics/

## How does the trusted kernel work? Why is elaboration sometimes really slow and sometimes not?

## What kind of indexed propositions can be made?

i.e. different propositions indexed by some function that we evaluate and then use. I realize this essentially identifies something like the "set of decidable identities", but would that even be useful?

## How do you write actual tactics?

## What is the choice function?

## How can you tell if a proof requires classical logic?

If a theorem or definition makes use of Quot.sound, it will show up in the #print axioms command.

```lean
def test3 : ℝ := 0
#print axioms test3
```

Returns
```
'C06S01.test3' depends on axioms: [propext, Classical.choice, Quot.sound]
```

## What are inductive type classes? Is that a normal thing?

## What's up with @[] stuff? Are those macros?

Those are attributes. You can see a list of them with `#help attr`.

## How does simp and ext work under the hood?

## How does @[to_additive] work?

## What libraries are essential?

https://github.com/Paper-Proof/paperproof ?

## What do tests look like? Why do some lean repos have em?

## How does documentation work in lean?

## How does package versioning in lean 4.12 work?

## How do you benchmark or profile your proofs / programs?

It says there's info here: https://blog.lean##lang.org/blog/2024-10-3-lean-4120/

## How do floats work?

## What are lean widgets?

https://lean##lang.org/lean4/doc/examples/widgets.lean.html

## Why was lean created? What are its priorities?

## What popular things cannot be done in lean right now?

i.e. stuff from https://github.com/coq##community/awesome-coq?tab=readme-ov-file#libraries

From the IMO problems in https://github.com/dwrensha/compfiles

- Basic geometry problems

## What's up with all the universes and sorts and stuff?

```
universe u

#check @Eq.refl.{u}   -- @Eq.refl : ∀ {α : Sort u} (a : α), a = a
#check @Eq.symm.{u}   -- @Eq.symm : ∀ {α : Sort u} {a b : α}, a = b → b = a
```

They seem to clutter up a lot of definitions in Mathlib.

They allow you to parameterize over all types in a way that
just using Type doesn't. See Type*. Sort 0 is the Prop. In Lean 3, `Type 0` was used instead of `Sort 0`.


## What parts of mathlib are the most incomplete?

- computational game theory
- usable contours for integration in complex analysis

## What is congArg

## What does this (· ▸ hx) mean?

## What's the difference between Array, List or scimath's DataArray?

Also Vector, List.Vector, etc.

## What is Batteries for?

## What are Lean.Environment's for?

## What is Type*?

The syntax variable (X Y ... Z : Type*) creates a new distinct implicit universe variable > 0 for each variable in the sequence.

Type universes help separate data from Prop.

## How are Sets defined?

Data.Set.Defs for the Set builder notation elaborator for syntax of the form {x | p x}, {x : α | p x}, {binder x | p x}.

Sets are an "abstraction barrier" around propositions of a type, so
`s : Set \alpha` is a barrier to `s : \alpha \to Prop` which uses
membership instead of application. On top of this, you can build
a preimage, image, etc API. You can then have coercion to sets using
the type class SetLike. We can go from a predicate to s using set builder
notation. We can go from `x \in s` into `s x `using `simp`.

```
--subtype
example : {x : ℤ // x ≥ 0} := ⟨1, by norm_num⟩
example : {x : ℤ // x ≥ 0} = Subtype (fun x : ℤ ↦ x ≥ 0) := rfl
```

```
--subset
example : {x : ℤ | x ≥ 0} := ⟨1, by norm_num⟩
example : ((fun x : ℤ ↦ x ≥ 0) : Set ℤ) = {x : ℤ | x ≥ 0} := rfl
```

SetLike implies CoeSort which is used to go from a set to a subtype

From Data.SetLike.Basic:

```
namespace SetLike

variable {A : Type*} {B : Type*} [i : SetLike A B]

instance : CoeTC A (Set B) where coe := SetLike.coe

instance (priority := 100) instMembership : Membership B A :=
  ⟨fun p x => x ∈ (p : Set B)⟩

instance (priority := 100) : CoeSort A (Type _) :=
  ⟨fun p => { x : B // x ∈ p }⟩
```


Set builder notation also has several forms. In defining a conjugate subgroup
carrier set, these two are equivalent.
```
  --carrier := {a : G | ∃ h, h ∈ H ∧ a = x * h * x⁻¹}
  carrier := {x * h * x⁻¹ | h ∈ H}
```

## What are "motives"?

Recall that the role of the motive is to specify the statement that we want to prove about the parameters of the recursive function.

https://lean-lang.org/blog/2024-5-17-functional-induction/

This is usually the thing we are trying to prove, and it is typically inferred.

## What is "Verso"?

- markdown variant in lean files
- should translate into html, latex, etc
- should support different "genres" like tutorials, examples, etc.
- should connect with docgen4 to do user documentation

Documentation is sparse, so I don't know what it is actually for.

https://github.com/leanprover/verso
https://www.youtube.com/watch?v=dv_vmVs3SQQ&list=PLlF-CfQhukNmfMTzyvM5eXgugSzp9A5UB&index=26

## What are quotient types?

## How do you use quotient types?

Seems like there's a lot of map\_3, sound, refl whatevers.
Also Setoids.

## What is Trunc?

## What is `@[elab_as_elim]` about?

See Finset.induction_on

I believe that in some things that act like case-work or induction, i.e. proving a general proposition through
a process, this seems to be used to support higher-order unification. If this is true, then replacing it without the `elab_as_elim`
will cause a lot of problems.

## Why are there `HEq` and `HAdd`?

Is this some fancy type perversions?

## How does dot notation work?

Sometimes you can avoid writing a module, sometimes the ordering is dependent
on the function definition?

```
inductive S | a | b | c

open S

def myElement : FreeGroup S := (.of a) * (.of b)⁻¹
```

```
example {M N P : Type*} [AddMonoid M] [AddMonoid N] [AddMonoid P]
    (f : M →+ N) (g : N →+ P) : g.comp f = AddMonoidHom.comp g f :=
  rfl

example : [1, 2, 3].map (· + 1) = List.map (· + 1) [1, 2, 3] :=
  rfl
```

```
/-- Composition of monoid morphisms as a monoid morphism. -/
@[to_additive "Composition of additive monoid morphisms as an additive monoid morphism."]
def MonoidHom.comp [MulOneClass M] [MulOneClass N] [MulOneClass P] (hnp : N →* P) (hmn : M →* N) :
    M →* P where
  toFun := hnp ∘ hmn
  map_one' := by simp
  map_mul' := by simp
```

```
/--
`O(|l|)`. `map f l` applies `f` to each element of the list.
* `map f [a, b, c] = [f a, f b, f c]`
-/
@[specialize] def map (f : α → β) : List α → List β
  | []    => []
  | a::as => f a :: map f as
```

## How does `deriving` work?

## Why is there Quot and Quotient?

Should you never you Quot?

## Can you do induction on hashmaps?

i.e. prove that something is true for all hashmaps by proving
it is true for empty and for a single insert

## What is the right way to connect approximation bounds?

- interval analysis
- attached propositions
- subtypes

## What progress is being made in verification of software?

like https://github.com/draperlaboratory/ELFSage.git
or https://github.com/leanprover/LNSym

## What would representing the scientific method look like in lean?

- https://reservoir.lean-lang.org/@lecopivo/scilean

## Can you create orphan type classes like in Haskell?

The example we have just given is dangerous, because Lean’s library also has an instance of Group (Equiv.Perm α), and multiplication is defined on any group. So it is ambiguous as to which instance is found. In fact, Lean favors more recent declarations unless you explicitly specify a different priority.

## Why have redundant information in a structure?

From 6.2 of mathematics in lean:
> For reasons we will explain later, sometimes it is useful to add redundant information to a structure, so that there are additional fields for objects and functions that can be defined from the core data.

## How do you use . notation in lean? Is it a namespace thing?

## What is <|?

## What are the best unknown features of the lean extension?

- Pinning types? How do you pin theorem names??
- Loogle?

## Is there a way to determine what simplifiations were applied in `simp`?

Sometimes it's unclear what assumptions are being used.

## What are the best practices with respect to type classes?

Things to remember:
- Type classes are just structs with special inference rules (+ extension rules)
- Extensions just include the subthing as a field and mark the projection
to that field as an instance. So the following will create an instance for SMul

```
class Module₁ (R : Type) [Ring₃ R] (M : Type) [AddCommGroup₃ M] extends SMul₃ R M where
```

whose field for SMul is the instance `Module₁.toSMul₃ : {R : Type} →  [inst : Ring₃ R] → {M : Type} → [inst_1 : AddCommGroup₃ M] → [self : Module₁ R M] → SMul₃ R M`.
- If there are two ways to define an instance during inference, then it is best practice to ensure that they lead to the exact same data. Props are safe since there
is only one way to populate it, but functions are not.

Questions:

For some reason, the whole diamond thing suggests that propositions and data
should have convenient defaults as low as possible, so they can overriden with more convenient definitions whenever a user expects a certain implementation.

I believe this is the whole "forgetful inheritance" thing, but honestly
that still doesn't make sense to me.

Some best practice tends to say "If you could do this with a poorer structure,
do it there, because otherwise the type classes on your structure could lead to shitty definitions".

- Why do you need to add a bunch of iff rules for default definitions
of type classes? Is this a reflection of the fact that any default
implementation should be equivalent to a supplied definition?

- what's the difference from having a simpler structure as a field vs an input argument? Why would you choose one or the other
- how are mixins used in lean? Are they?
- does this problem occur without type classes?
- why do you need to add this extra information at a lower layer?
- don't you still have times where a structure can be assigned in two ways in non isomorphic ways?

## What is a list of all the important attributes?

There's `#help attr`

## Why would you want to use attribute `@[class]` instead of `class`?

See the definition of a normal subgroup

## What's the right way to use external tools?

- i.e. automated theorem provers such as Duper, Vampire, Z3, CVC

It seems expensive to run it every time to rediscover it during the
build process. This could be true for ordinary lean tools. How should we use tactics which are expensive, probabilistic, etc?

### Equational theories uses Vampire

Either constructs explicit counter examples or uses a custom script to
just generate the appropriate tactic based proof. Seems hacky

### `bv_decide` using CaDiCal

Uses a very fancy reflection principle to create a little theory of circuit stuff, complete with expressions and a "compilation" stage, and then passes the proof certificate to an interpreter of that theory.

### `polyrith` with SageMath

Seems to replay the tactics directly from metaprogramming?

Also insanely uses the sagemath web API!

## When is it appropriate to use an "expensive" tactic?

## What's the rules on ' for map_mul, map_add?

Chapter 7 doesn't have anything on things like `LinearMap.map_mul'`.

## When is DecidableEq required?

the universal property of direct sums requires decidable equality on the indexing type (this is somehow an implementation accident).

## Is the tactic continuity still slow?

Lean 3 circa 2020 it was deifnitely running into a lot of problems.

## What's the best ways to debug slow tactics?

## What's the best way to format lean code?

- No linter!
- Lean 3 had snake case for terms. Lean 4 has lowerCamelCase for terms.
- But mathlib is very snake case!

## How is ∀ᶠ of a principle set so easily proved?

```
example {s : Set X} {p : X → Prop} : (∀ᶠ x in (𝓟 s), p x) = ∀ x ∈ s, p x :=
  rfl
```

My guess is that this is definitional equality... somehow.

## How as a user can you tell when you two things are definitionally equal?

Filter.Eventually is a case when `unfold` and `dsimp` does not make this obvious. See above.

## Is using `simp` a bad thing?

## Why are there so many ways to use induction?

- strong induction of various types
- recOn?
- induction tactic
- induction' tactic
- Nat.le_induction
- version from Theroem Proving in Lean???

## Is there any point in things like induction'/induction/rcases when you have refine?

It seems like refine can do almost everything???

## What is the bind operation on the Filter monad useful for?

Nothing comes immediately to mind.

Essentially how it works is that if you have a filter on \alpha and a map \alpha to Filter \beta, then something lives in the bind if "evenutally on alpha, you are in the beta filters"

```
@[simp]
theorem eventually_bind {f : Filter α} {m : α → Filter β} {p : β → Prop} :
    (∀ᶠ y in bind f m, p y) ↔ ∀ᶠ x in f, ∀ᶠ y in m x, p y :=
  Iff.rfl
```

https://github.com/leanprover-community/mathlib4/blob/ba7312b5aafeb13465bb9283536208f2158e6150/Mathlib/Order/Filter/Basic.lean#L1381-L1383

## What are all the tactics with a ? at the end?

- `aesop?` Tells you what worked with `aesop`. Usually same as `simp?`
- `aesop_cat?` aesop with Category Theory
- `arith_mult?` ???
- `apply?` looks to refine it (note you can pass `using` with a list of identifiers that must be used)
- `change?` suggests definitional equivalent goal of change
    - Using `change? term` where term has type holes fills in the gap!
    - especially useful for skipping steps where everything is definitionally equal
- `congrm?` proof widget for generating congrm call.
    - didn't work for some reason?
- `continuity?` suggests proving Continuous
- `conv?` seems to say it can suggest how to zoom in
    - I couldn't get it to work.
- `dsimp?` does seem to occasionally include things about gt or le
- `exact?` searches for exact match
- `finiteness?` solves goals in extended reals
- `forward?` part of aesop
- `gcongr?` has similar problems to congr? in large expressions with || and floor and all that
- `have?` with `using` and a type parameter, you can have it search like exact
- `hint` (honorable mention suggesting a tactic)
- `measurability?` for solving measurability goals
- `observe?` constructs a have with `exact?`. It's meant to replace `observe`
- `polyrith` (honorable mention suggesting a linear combination)
- `rw?` can search for rw rules
- `rw_search` will search for a sequence of rw rules using edit distance
- `saturate?` part of aesop
- `simp?` tells you what to simp with. There are also forms for many other simps
- `unfold?` usually gives you a weird rw command for unfolding a term

## What tactics are really worth reading all about?

Reading the card explains the card

- `wlog` read the whole thing about `generalizing`. Otherwise it generalizes everything!
- `apply at` can do forward reasoning at a hypothesis
- `apply_fun` works with inequalities using mono, injective, etc.
- `change a` changes goal to a using definitional equality
    - `change a with b` changes a to b everywhere assuming definitional equality (also with `at`)
- `checkpoint` will let you checkpont the state of tactic
- `congr` with variants `congr!`, `congrm` and the most important one
- `convert` should almost always be using a small number. It also wraps `congr!`
- ??`convert_to` is like `convert` but changes the type instead of the term
- `dsimp`
- `dsimp!` (unfolds harder)
- `eta_expand` and `eta_reduce` to unfold and apply functions
- `ext` can be applied only a few times with `ext : 1`
- `extract_goal`: extracts goal as a lemma and puts it in info view
- `extract_lets`: cases for lets. Introduces a variable from a let clause
- `gcongr`: congr plus inequalities
- `hint` suggests a tactic to use
- `induction` with `using` allows you to use any goal with `C t`
- `injection` solves goals like `a::b = c::d` by asking `a = c` and `b = d` seems similar to `congr` or `cc`
- `intro` can be combined with pattern matching exactly like `match`
- `introv` can be used to provide default names to stuff sometimes
- `lift` allows you to substitute `β` for `α` with a `CanLift α β` like substitute
 an integer with a natural number in a way that's dual to `zify`
- `lift_lets` move `let` to as far out as possible
- `mono` applies monotonicity goals
- `move_add`, `move_mul`, and `move_oper` let you move atoms to the left or right like
  `b + c + d + a` to `a + b + c + d`
- `nlinarith` adds more things to the context than `linarith`
- `norm_cast`
- `norm_num`
- `observe` calls have with `exact`
- `obtain` is have with match
- `peel` does what looks like a combo of intro, refine on exists, and application of a term all at once
- `plausible` tries to find a counterexample by testing 100 things.
- `positivity` tries to solve goals comparing with 0
- `push_cast` the opposite version of `norm_cast`. Pushes coercion inwards.
- `push_neg` pushes neg inward
- `qify`: converts integer stuff to rationals
- `rcases` has special cases for implicit and quotient types too
- `rcongr` congr + functions
- `refold_let` opposite of unfold
- `rel` sort of like gcongr except you give it the lemmas
- `rename` renames hypothesis matching a type placeholder
- `rename_i` renames last inaccessible things
- `replace` is have but it can replace an existing hypothesis
- `rify` converts goal to one over real numbers
- `rsuffices` is a combo of suffices and obtain
- `rw_search` tries to solve by applying `rw` automatically
- `save` is checkpoint for afterwards
- `set` has literally nothing listed?
- `show_term` tells you how you could have done it with exact
- `simp` also has `simp_arith` and `simp_rw` (recursive rw) or even `simp_all` and `simpa`
- `specialize` lets use a hypothesis and apply it with some arguments
- `split` has variants for `and` and `if`
- `subst` substitutes a value in the goal using a hypothesis (or a given expression)
  has variants `subst_eqs` repeatedly tries `subst` for all equalities,
  `subst_vars` for variables and `substs` for applying to all hypotheses
- `suffices` you can rename the `this` with `suffices h : `
- `tauto` (and `itauto`) sovle things that are all exists, ands, ors, and iffs.
- `tfae_have`, `tfae_finish` let you prove a list of things are equivalent more easily (see TFAE)
- `use` has `use!` which lets you flatten more
- `with_panel_widgets` will show widgets!
- `zify` converts things to integers

### Extras not under Mathlib.Tactic

- `fun_prop` for proving differentiability or continuity

### Commentary

I didn't know about `apply at` , `move_add`, or how many special cases there are for `apply_fun`.  The whole suite of zify, qify, and rify would have saved me some time when doing a bunch of conversions.

Some of them surprised me, but I didn't find a use for them, like `nlinarith`, `observe`, `checkpoint`, `rw_search`, and `show_term` (Matt, you may find that one interesting especially).

Some of the more proof widget ones like `congr?` or `unfold?` didnt work right many times in very unpredictable ways.

I think there's a lot more searching ones than expected: like you can do #leansearch, #loogle, #find, and #moogle in a bunch of places to search for terms.

`extract_goal` would have saved me a bit of time overall when refactoring.

There's also a few I discovered quickly that became extremely routine: `gcongr`, `positivity`, and `wlog` have been used and perhaps overused.

## What are some helpful command?

- `#leansearch`
- `#loogle`
- `#find` tries to pattern match using local stuff #find _ + _ = _ + _
- `#moogle`

## Do we have the equivalents of geometric invariance wlog tactics?

See https://www.cl.cam.ac.uk/%7Ejrh13/papers/wlog.pdf

## How often should you be tracing tactics?

## Are tactics generalized proofs?

Again from the wlog paper https://www.cl.cam.ac.uk/%7Ejrh13/papers/wlog.pdf

"This is all very well, but the process is quite laborious. We have to carefully apply
translation to all the quantified variables just once so that we don’t get into an infinite
loop, and then we have to appeal to suitable basic invariance theorems for pretty much
all the concepts that appear in our theorems. Even in this case, doing so is not entirely
trivial, and for more involved theorems it can be worse, as Hales [5] notes:

> [. . . ] formal proofs by symmetry are much harder than anticipated. It was nec-
> essary to give a total of nearly a hundred lemmas, showing that the symmetries
> preserve all of the relevant structures, all the way back to the foundations.
>
> Indeed, this process seems unpleasant enough that we should consider automating
> it, and for geometric invariants this is just what we have done.
"

This Hales' paper is T. C. Hales. The Jordan curve theorem, formally and informally. The American Mathematical
Monthly, 114:882–894, 2007.

## How do we build good "wlog" like arguments?

Again from the wlog paper https://www.cl.cam.ac.uk/%7Ejrh13/papers/wlog.pdf

- **Higher order logic theorem** of how all casework can be reduced to a specific case plus some sort of invariance side goal
    - Proving the theorem of this correctness is often oddly easy by quantifying over propositions
    - `elab_as_elim` might be required as an attribute to support higher-order unification in Lean
- Prove a ton of **invariance theorems**! (Is this how `norm_cast` came about?)
- Write a tactic to **automate the search and application** of these invariance theorms.
    - In particular, we want to find versions of the theorem which are "nice" (like 0, or come from a 2d plane).
    This is something I really don't understand from the paper.

### Speculation

This invariance viewpoint is actually quite prescient, since its really only a hop, skip, and a jump
to a more Kleinian viewpoint on how symmetry can be used in mathematics. What matters _is_ the symmetry and
in some sense, the theory is defined by it. This way of making the statement precise is odd though. I would
like to phrase this statement in a rather different way: geometric facts are true for all objects isomorphic
under some geometric operations. Just like set theoretic facts are statements which are true for the
class of objects isomorphic to the operations on set theory.

Suppose `X` is an object in the category of sets. Note that this is not up to isomorphism.
For all transformations `f : Y -> X`, I would like that `P Y \to P X` but usually this sort of operation
doesn't usually make sense. We don't typically quantify propositions over types like this. It turns
a proposition into a functor somehow?

### Should it be reflective theorems?

From the same paper we have the following quote

> Another interesting idea would be to reformulate the process in a more ‘metalogical’ or ‘reflective’ fashion, by formalizing the class of problems for which our transformations suffice once and for all, instead of rewriting with the current selection of
theorems and then either succeeding or failing. From a practical point of view, we think
our current approach is usually better. It is actually appealing not to delimit the class of
permissible geometric properties, but have that class expand automatically as new invariance theorems are added. Moreover, to use the reflective approach we would need to
map into some formal syntax, which needs similar transformations anyway. However,
there may be some situations where it would be easier to prove general properties in a
metatheoretic fashion. For example, a first-order assertion over vectors with M vector
variables, even if the pattern of quantification is involved, can be reduced to spaces of
dimension ≤ M [9]. It should be feasible to handle important special cases (e.g. purely
universal formulas) within our existing framework, but exploiting the full result might
be a good use for metatheory

Something like the tactic `grind` or `bv_decide` appear to be using reflection based approaches in a way Lean makes more approachable. Similarly, Duper might be doing the same thing, maybe? Translating the general to special case transformation into some suitable theory (like an SMT) may require a lot of effort still, and it's unclear if it's
possible.

## What are "heartbeats" in lean and can we prove things about them?

## How do I get my struct to display nicely in proof?

- Override pretty printing with the delaborator which can be done with `notation`
- using custom `notation`?
- implement `ToExpr` (described at https://leanprover-community.github.io/archive/stream/113488-general/topic/Notation.20for.20the.20Lean.20InfoView.html) (example at https://github.com/leanprover-community/mathlib4/blob/68ea7d25dcbdd9f46072adf8927a684d4194abbb/Mathlib/Tactic/Sat/FromLRAT.lean#L64-L68)

## What is the delaborator?

It seems to be part of the pretty printer which takes proof state and
puts it back from expressions to syntax (like in the infoview).

## What is the difference between syntax and expressions?

## What is the right way to do unit testing?

The closest I've seen so far is https://github.com/argumentcomputer/LSpec

I've seen the following though:
https://github.com/leanprover-community/mathlib4/blob/a1c2c42dff9005625384455a1415142d7fb04a98/lakefile.lean#L156

Mathlib has a test executable to check that you can run something while importing mathlib
They have a lot of compile-time tests like https://github.com/leanprover-community/mathlib4/blob/master/MathlibTest/ApplyAt.lean where they use some "guard_" tactic to guarantee the hypothesis or goal is in a particular form

Lean proper has a bunch of tests for compilation and all that, but it seems like a red herring https://github.com/leanprover/lean4/tree/master/tests

## How fast is the interpreter (#eval)?

Does it use compiled code in other libraries? Yes:

> Before emitting
> C code, we erase proof terms and convert Lean expressions into an intermediate
> representation (IR). The IR is a collection of Lean data structures,12 and users
> can implement support for backends other than C by writing Lean programs
> that import Lean.Compiler.IR. Lean 4 also comes with an interpreter for the IR,
> which allows for rapid incremental development and testing right from inside the
> editor. Whenever the interpreter calls a function for which native, ahead-of-time
> compiled code is available, it will switch to that instead, which includes all func-
> tions from the standard library. Thus the interpretation overhead is negligible
> as long as e.g. all expensive tactics are precompiled

https://link.springer.com/chapter/10.1007/978-3-030-79876-5_37

Does it use compiled code otherwise?

Yes according to #help command under eval.

Is it fast?

It's a bit slower than Python for computing the sum of 10^8 integers by a factor of around 2x.

## What does transparency refer to in lean?

https://leanprover-community.github.io/lean4-metaprogramming-book/main/04_metam.html#transparency

Transparency refers to how far `reduce` will reduce expressions to. When using
a lot of tactics, they will try and unfold these definitions to some level
of transparency, and this can often be overriden if desired.
Usually only tactic writers need to care about this. Since it
involves partially evaluating code in a kind of shitty interpreter.

It's unclear when you would need to change from the default as a user.

### Default

Reduces everything except `@[irreducible]` definitions.

### Reducible

Reduces only `@[reducible]` / `abbrev` definitions.

### Less important ones

- all
- instances (reducibles + instances)

## How do you extend tactics?

## What are syntax categories?

I know you can see them with #help cats

## What are the best proof widgets?

Are there any good geometry ones? (Geometry is a weakpoint).

## What is the QED Manifesto?

Seen in relation to the wlog paper and in relation to this paper about sledgehammers

https://people.mpi-inf.mpg.de/~jblanche/h4qed.pdf

## What is Duper?

Proof reconstructor that uses given lemmas to search for a proof

## What is a hammer?

The terminology hammer seems to come from Isabelle's "Sledgehammer"
and refers to tools that use automated theorem provers to prove a theorem.
Essentially they are a form of program synthesis. They usually refer
to symbolic AI that is running on your computer.

They typically have three parts: premise selection, automated theorem proving,
and proof reconstruction.

As mentioned in Jason Rute's The Last Mile, the "Magnus Sledgehammer"
works with only the premise selection part somehow.

## What is the best way to do premise selection?

### Old School k-NN

Apparently used by TacticToe, HOList, and Tactician. This should
be the baseline.

- Record hand-crafted features for each proofstate-goal
- Use k-NN to look up closest proofstates
- Tree search with tactics at those proofstates

- Can be done online and locality sensitive hashing can be used to search over
all tactics?

## How do you do typed tables?

https://users.cs.utah.edu/~blg/publications/table-types/lgk-pj-2022.pdf

https://cs.brown.edu/media/filer_public/b8/d7/b8d70bb1-9c0e-467f-aec6-aaf42019f169/rotellajoseph.pdf

https://researchseminars.org/talk/LeanTogether2025/13/

## How do you use `aesop`?

It's highly configurable... What does it do by default?

## How does `bv_decide` work?

`bv_decide` is highly inspired by this SMT solver Bitwuzla, which implements
its own tricks, doesn't formally verify, no type checking, etc.

Summary of https://researchseminars.org/talk/LeanTogether2025/16/

1. Proof by contradiction
2. Processing using normal tactics
3. Reflection: create a term-level experssion and interpreter and show equivalence with rfl. Is this "Reflection" the same as Expr internalization?
4. Bitblast the expression: Turn it into a bunch of boolean circuits.
5. Obtain "UNSAT" certificate from SAT solver
6. Use `ofReduceBool` + certificate to show that a `verifyBVExpr` is False and then prove that `BVExpr.Unsat`.

### Similar Tools

Performance is quite comparable to Bitwuzla

- HOL Light has a CaDiCal (the SMT solver) bitblaster
- SMTCoq: This is worse, since it just replaces the proof directly instead of
creating a checker like `bv_decide`'s `verifyCert`, so is particularly slow with long
proofs.
- CoqQFBV: They wrote a SMT sovler written in Coq, go to OCaml, and run there.

## What is `ofReduceBool`?

Showing `f x = true` can actually be quite difficult even if `x` is concrete.
This reduction step can take a long amount of time, and even if the lean compiler
can optimize, the definitional equality checker make take a lot longer except
for specific cases like with good structural recursion guarantees and Nats (e.g. https://github.com/girving/interval/blob/9d1e966a727b0429023fb776244d03d5078c9953/Interval/Misc/Nat.lean#L442). Lean's verifier does something much simpler by using
a more restricted model of computation.

The axiom `ofReduceBool` solves this by putting `f x` in an opaque object `reduceBool`. The lean reducer then reduces this by compiling and running `f`. Then the axiom
`ofReduceBool` says ok it's a real equality too.

```lean
axiom Lean.ofReduceBool (a b : Bool) (h : Lean.reduceBool a = b) :
a = b
```

Is this unsound? Yes almost certainly. It trusts the computer and compiler with
an extra 30k and growing lines of code. It also means these proofs
can't be checked by external checkers.

This is used in `native_decide` and `bv_decide`.

## How do I make a "heterogeneous" function?

By this, I mean something like the following. I have an index set,
for each index, I have a type and an element of this set.

Can this be used to replace heterogeneous lists in a dependently typed table?

## Can you interpret a model with a partial function to prove it?

Reflection principles let you lift proofs from an interpeter to a model
to elements modeled, such as bitvectors by boolean circuits expressions.
Can you use `partial_fixpoint` to use a potentially non-terminating
interpreter to provide a proof?

Obviously this is a bad idea. Ideal proofs should be checkable in polynomial time after
the fact, but there may be some instances where you don't particularly care.

## What cool tools has Mario Carneiro made?

- What is `shake`?
- What is `lean4checker`?
- What is `lake exe refactor`?

## What is Decidable, really?

A Decidable is a type class, which obscures the actual meaning of it.
Decidable is a sum type of $\neg p$ or $p$. This is different from
a the other sum type $\neg p \lor p$. Decidable is a `Type`, but $\neg p \lor p$
is a Prop. Proof irrelevance doesn't apply to types, so we can actually tell
which one is which with pure computation.

One of the interesting things is that there's another way to do this.
If a specific `P x :  Prop` can be computed instead as a function `f` returning a Boolean
indicating true or false, and we can prove equivalence between `P x` and `f x = true`,
then we can construct the decidable term with `f`.

I'm still quite fuzzy on the details of how the type inference works here to make
`f x = true` decidable, and I'm unwilling to say by definition.

## How is `f x = true` decidable for computable `f`?

What internal mechanism of lean is at work here to do this madness?

See the decidability and definition of `LT` for `Rat` for what I mean.

## What's better for "proof reconstruction": proof replay or reflection?

Suppose you have a set of lean hypotheses and a lean goal. You want to
convert this to a problem in some SMT, pass it to a solver, and get it back.
Obviously, you need to somehow embed your logic into the logic of the theory. This
is a **shallow embedding**: meaning you are exploring the logic of your system
in another theory via reimplementation. This has a ton of preprocessing.

The solver will then spit out some certificate. Now once you have a proof
certificate, you have two options: proof replay and verified checking / reflection.

As much as I dislike the talk, I learned a bunch of this from Lean Together
2025 talk on lean-SMT.

### Proof Replay

In proof replay, you convert the certificate into a proof that can be used
in lean. This is usually a sort of shallow embedding. You are implementing
their proof rules in your system again.

Some proof "rules" may be "tactics" in Lean. Or you may even need
to make a proof rule into a reflection procedure.

Isabelle SledgeHammer does proof replay, which has been by hearsay
quite successful. It seems to be Isabelle's most powerful tool.

The reported pros and cons (from the Lean Together 2025 lean-SMT talk) are
(1) easier to modify and implement and (2) slower. Essentially, it seems
hacky, which might reflect the ad-hoc interfaces to SMT certificates
more than anything else.

Additionally, most lean tactics use a form of proof replay even
when computations are in Lean:
- `ring`, `group`, `field_simp`
- `linarith` (even though computation)

### Reflection / Verified Checking

A verified checker does "reflection rathen than simulation". See
"How does `bv_decide` work?" for more details.

SMTCoq apparently works this way, which is strange, since isn't Coq slow?

`lean-auto` is another SMT-like hammer in Lean that works this way, seems
related to Duper too, but this is all very confusing.

## What common libraries in Coq and Isabelle are missing from Lean?

## Is it worth using LeanSSR?

It looks maybe faster if less readeble? https://arxiv.org/abs/2403.12733

I'm not convinced its usage of reflection / reduction rules makes sense. It
seems like a less convenient way to define simp rules.

## Why does Lean Expr have `proj`?

From the metaprogramming book:

> - `proj` is for projection.
>  Suppose you have a structure such as `p : α × β`,
>  rather than storing the projection `π₁ p` as `app π₁ p`, it is expressed as `proj Prod 0 p`.
>  This is for efficiency reasons ([todo] find link to docstring explaining this).

## What is `Qq`?

[`Qq`](https://github.com/leanprover-community/quote4) AKA `quote4` refers to the
(quasi-)quoting library in Lean 4. It is heavily used and documentation is provided
right by mathlib docs.

## How do I use `Qq`?

There doesn't appear to be any serious documentation, tutorials, or talks.

## What doesn't `Qq` work with?

It doesn't work with universe polymorphism, since
that occurs at a different level.

Dependent types?

## What is the `LocalContext` without a `withContext`?

In the `MetaM` monad, `getLCtx` returns a `LocalContext` which
can be set with `withContext`. It appears to be a generic Reader-like
monad. It's unclear what the starting config is. It _seems_ to be
wherever the `MetaM` is called from, so do you even need
to assign `withContext`?

## What doesn't "Metaprogramming in Lean 4" use `Qq`?

## How are supposed to search for answers to lean questions?

- Google with "lean prover" or "lean 4" or "lean lang"
- Lean 4 Reference Manual
- Zulip search directly. (Not indexed by google after December 2023)
- Mathlib autogenerated docs (+ Aesop, Batteries, Lean, Qq, etc.)
- leansearch, moogle, loogle for finding a part of the source

## Can you write a `reduce` function which treats specific functions differently?

Say for instance, you want to reduce and unfold all constants expect `List.append`. Could you
do that?

It seems the answer is no according to the "Metaprogramming in Lean 4" book. About
a [similar problem](https://leanprover-community.github.io/lean4-metaprogramming-book/main/04_metam.html#weak-head-normalisation)
of destructuring all $\land$ as a sort of "deep matching":

> This sort of deep matching up to computation could be automated. But until
> someone builds this automation, we have to figure out the necessary `whnf`s
> ourselves.

## Why is `resetDefEqPermCaches` needed in `isExprDefEq` in `isDefEq`?

It seems to hurt performance, and there's both checkpointing
and a cache in this function?

## Why does the LocalContext of a main goal include the theorem itself?

It's defined as an implementation detail. Whose?

## When should you use `notation3` over `notation`?

This came up in a Zulip conversation.

## How do asympotics work in Lean?

## When can you use mkAppM?

Only when you are applying it with and to expressions without
bound variables.

```lean
def doubleExpr₁' : MetaM Expr := do
  pure (.lam `x (.const ``Nat []) (← mkAppM ``Nat.add #[.bvar 0, .bvar 0])
    BinderInfo.default)

#eval show MetaM Format from do
  ppExpr (← doubleExpr₁')
```
This errors at runtime with
> unexpected bound variable #0Lean 4

## When do overlapping notations fail?

From the "Metaprogramming in Lean 4" book:

```lean
notation:65 lhs:65 " ~ " rhs:65 => (lhs - rhs)
notation:65 a:65 " ~ " b:65 " mod " rel:65 => rel a b
```

> Lean will prefer this notation over parsing `a ~ b` as defined above and
> then erroring because it doesn't know what to do with `mod` and the
> relation argument:

Unfortunately, this doesn't seem to be true. In fact, it parses both correctly:

```lean
#check 0 ~ 0 mod Eq -- 0 = 0 : Prop
#check 0 ~ 0 -- 0 = 0 : Prop
```

## How do you prove things about computational complexity in Lean?

## What is the `by?` term / tactic?

## Is there a tutorial on extending tactics like `positivity`, `gcongr`, and `simp`?

# Lean Answers

## What's this symbol ⦃ ?

Abbreviation for `{{`

First of all, it allows you to use implicit arguments in a different way.
So normal implicit arguments, no matter where they are, will try and grab whatever hypotheses they need.
But sometimes, like with injective, you only want it to grab the implicit argument if you are calling with an implicit argument after the fact.
So if h : Injective f, then using h in a context will not try and grab x and y. It will only try and grab x and y when you give it the next implicit argument h (f x = f y).


Sources: https://leanprover.github.io/theorem_proving_in_lean4/interacting_with_lean.html
>  Lean offers a weaker annotation, {{y : Nat}}, which specifies that a placeholder should only be added before a subsequent explicit argument. This annotation can also be written using as ⦃y : Nat⦄, where the unicode brackets are entered as \{{ and \}}, respectively.

http://pixel-druid.com/articles/weakly-implicit-arguments-in-lean.html

List of all abbreviations: https://github.com/leanprover/vscode-lean/blob/master/src/abbreviation/abbreviations.json

## What is "leanblueprint"?

It allows you to state a bunch of things in LaTeX
(really plasTeX) and then say this is proved
in lean at some location.

It can automatically create a dependency graph telling you which theorems can be proved, are proved, or aren't even stated yet.

It's typically used for things like "proving FLT".

Github CI integration is good.

https://github.com/PatrickMassot/leanblueprint

## What does that one uparrow before a symbol mean?

Coercion

https://proofassistants.stackexchange.com/questions/4113/how-to-perform-type-conversion-coercion-in-lean-4

- `exact_mod_cast`: exact with casting
- `apply_mod_cast`: apply with casting
- `push_cast`: push casting inward as possible
- `norm_cast`: normalize expresion with cast. Can often replace goals with uncasted versions
- `rw_mod_cast`: rw with norm cast

https://lean-forward.github.io/norm_cast/norm_cast.pdf or https://arxiv.org/abs/2001.10594

https://lean-lang.org/doc/reference/latest//Tactic-Proofs/Tactic-Reference/#tactic-ref-casts

- `lift` to replace variables with lowered version


In certain circumstances, we can coerce elements
of a type to a type themselves, or coerce a structure to a function: https://lean-lang.org/theorem_proving_in_lean4/type_classes.html?highlight=coercion#coercions-using-type-classes

## How do I document a function?

With a special docComment? Hover over a documentation after Ctrl-Click to get more info. TLDR:

```lean
/--
This is a doc comment
-/
def hi: Nat := 3
```

## What does f (R := K)?

This is named argument syntax. `f` has an argument named `R`. This is useful
for named implicit variables.

This can also indicate default values if used in the definition of a function.

## What is the .. syntax?

It's a bunch of implicit arguments. Very useful with named arguments!

```
example : V →ₗ[K] V where
  toFun v := 3 • v
  map_add' _ _ := smul_add ..
  map_smul' _ _ := smul_comm ..
```

https://lean-lang.org/lean4/doc/lean3changes.html#function-applications

In Lean 4, we can use .. to provide missing explicit arguments as _. This feature combined with named arguments is useful for writing patterns. Here is an example:

```
inductive Term where
  | var    (name : String)
  | num    (val : Nat)
  | add    (fn : Term) (arg : Term)
  | lambda (name : String) (type : Term) (body : Term)

def getBinderName : Term → Option String
  | Term.lambda (name := n) .. => some n
  | _ => none

def getBinderType : Term → Option Term
  | Term.lambda (type := t) .. => some t
  | _ => none
```


## What can Lean users do for AI in Lean?

From https://researchseminars.org/talk/LeanTogether2025/2/

- Try Cursor ( or continue.dev + VS code) with Sonnet 3.5 (best for Lean 4)
- Share experiences online or with AI researcher
- Keep lists of things that work or don't work in different systems

### Try out experimental tools

- Lean Copilot, LLMLean, LeanAide, Duper, Improver, etc. Paperproof?
- Try tools in other languages
  - Tactician, Graph2Tac, Coq Pilot, CoqHammer
  - Sledgehammer in Isabelle
- You have a GPU use it? (ReProver, DeepSeek-Prover, InternLM-Prover)
- Feedback on setup process
- Try running them for hours
- Write getting started tools
- Answer questions online like "are there any tools which can solve X?"
