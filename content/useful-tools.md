Title: Useful Tools
Date: 2018-11-08
Tags: linux, computing, list
Slug: useful-tools
Authors: Joseph McKinsey
Summary: Maximize your computer, if you have the time.
Category: Notes

Most of the tools here will revolve around linux, and most every one of them I
wish I had heard of earlier.

## Editors

- Visual Studio Code (if you just want something that works)
- IntelliJ for Java / Kotlin
- Spacemacs: the best of both Emacs and Vim, with loads of easy to install
  layers, from markdown to coq.
- Neovim: A forked version of vim, which is mostly just the same.

# Linux

## Distros

- Arch Linux (if you have the time, inclination, and friends available for configuring and debugging)
- Ubuntu (if you want stability)

## Command-Line Utilities

- `sl`: the steam locomotive
- `fish`: terminal as an alternative to `bash`.
- `zsh`: terminal alternative with backwards compatibility.
- `k2pdfopt`: format pdfs better for E-readers.
- `unzip`: does literally one thing.
- `feh`: an easy way to display a photo
- `transset-df`: change opacity on a window.
- `ImageMagick`: an all-in-one utility for doing everything from screenshots
  with `import` to making videos from images.
- `tree`: prints out a directory tree from the current working directory.
- `screenfetch`: displays system information.
- `htop`: command-line task manager.
- `ripgrep`: faster version of `grep` written in rust.
- `udiskie`: mount USBs in a better way.
- `network-manager`: the only nice way to do wifi.
- `redshift`: make your screen less blue at night.
- `elinks`: read the browser in your computer.
- `youtube-dl`: for grabbing videos or sound from youtube.
- `r2`: for decompiling.
- `GraphViz`: for drawing graphs (vertex-edge).

## Using the Computer

- `i3-wm`: for tiling windows.
  - `i3status-rs`: better `i3status` bar.
- `compton`: compositor for transparency, vsync, and more.
- `dunst`: notifications in the background.
- `rofi`: better way to open applications.
- `greenclip`: have access to clipboard history.
- `termite`: terminal emulator. If not available, try `terminator` or `konsole`.

## Disabling the CAPS-LOCK key with xmodmap

## Assorted Applications

- `thunderbird`: read mail. Allows a lot of simple use for filtering.
- `calibre`: organize books, articles, and news. Exports to e-readers.

## Plugins and Extensions

- Vim keybindings in a web-browser `CVim`, `Vim Vixen`, etc.
- Tree style tabs, along with disabling the tab bar on top.
- `ublock` for add block.
- `rainbow parenthesis` so you can actually read lisp.
- `powerline` so you can have a cool little arrow at the bottom of `vim`, `terminal` (with `zsh`), or `i3status-rs`.

### Emacs
- `org mode`: for better linking between files than Markdown. Great for notes.
- `AucTex`: LaTeX is now a readable language in emacs.
- `treemacs`: nice way to browse files.
- `magit`: the best way to use git. Beats terminal any day.

## Programming Languages

- LaTeX for formatting anything written: resumes, homework, projects, slideshows with `Beamer`.
  - `xelatex` for extra fonts.
- Python, but more importantly,
  - `numpy`, `scipy`, `matplotlib`, `jupyter`,
  - `BeautifulSoup`,
  - `Pillow`,
  - `requests`.
- R for anything statistics related.
- Racket for anything computational.
- Haskell if you want your program to be a beautiful butterfly.
- Sagemath
