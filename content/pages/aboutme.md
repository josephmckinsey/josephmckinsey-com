Title: about me
Summary: What I'm doing right now

# Resume and Such

## [Resume](https://gitlab.com/josephmckinsey/resume/-/blob/master/resume.pdf)

## [GitHub](https://github.com/josephmckinsey/)

## [GitLab](https://gitlab.com/josephmckinsey/)

## [LinkedIn](https://www.linkedin.com/in/joseph-mckinsey-356195146/)
 
# What I'm Interested In Right Now

This includes what I'm sort of interested in, very interested in, and
interesting things I know hardly anything about.

- Interactive Theorem Provers
    - Lean
- Scientific Computing in Julia
- SMT solvers. They should be more useful, right?
- Alternatives to data frames in data analysis
- Algebraic Topology
    - I finally want to learn about cohomology
- Applying category theory to scientific computing
- Bell's inequality
- Representation theory
- Audio Programming (but only a little bit)
- Jazz and classical piano and electronic keyboards
- Computational Geometry
- Numerical PDEs
- Interval Arithmetic and Numerical Analysis
- Game Theory
- Stochastic Modeling
- Scientific visualization (more than just matplotlib)
- Type theory
- Bayesian statistics
- Guassian processes (and generalized versions)
- Category Theory
- Wavelets
- Fourier analysis on locally compact abelian groups (and I suppose groups in general)
- Information Theory
- Lossy compression of time series data
- Rigorous fundamentals of pertubation theory
- Celestial Mechanics
- Sundials and their mottos
- Unconventional decision making tools and voting schemes
- Control Theory
- Ergodic Theory
- Convex optimization and linear programming
- Nonlinear optimization
- Trust regions and line searches in optimization (like Adam's method)
- General Relativity
- Calculus of Variations and classical mechanics
- Model Theory and applying reflection in theorem provers
- Symbolic Programming
- Algebraic Geometry
- Differential Geometry
- Computer Architecture
- Machine-learning based A* heuristics 
- Bayesian methods for active learning
    - early stopping for simulations
    - connecting machine learning
    - Importance sampling
- Markov Chain Monte Carlo
- Probability
- Measure Theory (especially that annoying stuff at the start of any book)
- Ito calculus and stochastic integrals
- Filtrations (the mathematical kind)
- Martingales (semi-, super-, sub-)
- Girih Tilings
- How Hardy-Weinberg is consistently taught incorrectly in statistics
- Bonferroni Inequalities and estimation of variance
- Transfer operators in statistical mechanics
- Shift space and symbolic dynamics
- Artwork of Channa Horwitz
- d-separation
- Vecchia approximations of guassian processes
- Rigorous Gauge Theory. Why is it so annoying?
- Simulation of Schrodinger like hyperbolic PDEs (dirac / proca-equation)
- Presheafs and sheafs
- C* algebras
- Lie groups and algebras. I would love to learn more.
- Principle curves and surfaces (statistics)
