Title: Beautiful Proofs
Summary: A collection of clever simple proofs.
Date: 2020-02-15
Modifies: 2020-02-28
Authors: Joseph McKinsey
Tags: proofs, math, list
Category: Notes

There are a [lot](https://mathoverflow.net/questions/28788/nontrivial-theorems-with-trivial-proofs/28791) of proofs whose clever simplicitly is worth writing down.

"... the snobbery of the young, who think that a theorem is trivial because its proof is trivial." -- 'the quote is a garbled version of Grothendieck, quoting Ronnie Brown quoting J.H.C. Whitehead. I found it on p.188 of the PDF version of Récoltes et Semailles'

- Lagrange's Theorem
- Bayes' Theorem
- Louiville's Theorem (the complex one)
- Uncountability of the reals
- Infinitely many prime numbers exist
- Union bound on probability
- Markov's Inequality
- Chebyshev's Inequality
- The theorem that there are two irrational numbers a and b with ab rational
- Poincaire Recurrence Theorem
- [q-norm <= p-norm if $p < q$](https://math.stackexchange.com/questions/367899/q-norm-leq-p-norm)
- [If in a list of $n$ vectors, each is within $\frac{1}{\sqrt{n}}$ of an orthogonal basis, then your vectors are independent](https://math.stackexchange.com/questions/1732753/proving-a-basis-for-inner-product-space-v-when-e-j-v-j-frac1-sqrtn).
- Proof of linear independence of $e^{a t}$.
- Double counting proof that $\sum_{i=0}^n \binom{n}{i}^2 = \binom{2 n}{n}$.
- Double counting proof that $\sum_{i=0}^j (-1)^i \binom{n}{i} = (-1)^j \binom{n-1}{j}$.
- Double counting proof that $\sum_{k=0}^n k \binom{n}{i}^2 = n \binom{2 n - 1}{n}$
- Proof that continuous bijections from a compact space to a hausdorff space is a homeomorphism.
- [Irrationality of 21/n for n≥3: if 21/n=p/q then pn=qn+qn, contradicting Fermat's Last Theorem. Unfortunately FLT is not strong enough to prove 2–√ irrational.](https://mathoverflow.net/questions/42512/awfully-sophisticated-proof-for-simple-facts/44742#44742)
- Proof of Euler Product Formula for the Riemann Zeta function ($\sum_{n=1}^\infty \frac{1}{n^s} = \prod_{p \in \mathrm{primes}} \frac{1}{1 - p^{-s}}$).
- Centroid proof of $\cos{\frac{pi}{7}} + \cos{\frac{3 \pi}{7}} + \cos{\frac{5 \pi}{7}} = \frac{1}{2}$.
