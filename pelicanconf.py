#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Joseph McKinsey'
SITENAME = 'Random Ramblings'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'America/Denver'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

LINKS = (('Resume', 'https://gitlab.com/josephmckinsey/resume/-/raw/master/resume.pdf'),)

# Social widget
SOCIAL = (('GitLab', 'https://gitlab.com/josephmckinsey/'),
        ('GitHub', 'https://github.com/josephmckinsey'),
        ('LinkedIn', 'https://www.linkedin.com/in/joseph-mckinsey-356195146/'))

DEFAULT_PAGINATION = 10

OUTPUT_PATH = 'public'

PLUGIN_PATHS = ['pelican-plugins']
PLUGINS = ['render_math']

THEME = 'pelican-blueridea'

STATIC_PATHS = ['images', 'extra', 'flean']
EXTRA_PATH_METADATA = {
    'extra/favicon.ico': {'path': 'favicon.ico'},  # and this
}

MARKDOWN = {
    "extension_configs": {
        # Needed for code syntax highlighting
        "markdown.extensions.codehilite": {"css_class": "highlight"},
        "markdown.extensions.extra": {},
        "markdown.extensions.meta": {},
        # This is for enabling the TOC generation
        "markdown.extensions.toc": {"title": "Table of Contents"},
    },
    "output_format": "html5",
}

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
